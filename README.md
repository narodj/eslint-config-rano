

## Adding New Rules
ESlint rules are split into different files depending on their purpose.
* NodeJS rules should be added to ```eslint.js```.
* React rules should be added to ```react.js```.
* React Native rules should be added to ```react-native.js```.

The rule should be added to the file with the correct granularity. If such a file exists, see __(2a)__, else see __(2b)__.

### (2a) Add the new rule to the file.
Within the file, add your rule somewhere within the ```rules``` object, preferably maintaining alphabetical order and in the correct section (if sections have been made). e.g. To add a rule ```no-undefined```:

```javascript
// Before
rules: {
    "no-shadow": 0,
    "no-unused-vars": 2
}

// After
rules: {
    "no-shadow": 0,
    "no-undefined": 2,    
    "no-undefined": 2
}
```

The number following the rule determines the response if it is violated.
* 0 turns the rule off, and will have no effect on builds.
* 1 turns the rule on as a warning, builds will not fail.
* 2 turns the rule on as an error, builds will fail if rule is violated.

In the majority of cases you will want to define it as 2.

### (2b) Create a new file and add your rule.
In the case where none of the files are appropriate, create a new js file with your rule matching the following syntax:
```javascript
module.exports = {
    extends: ["./react.js"],
    plugins: ["react-native"],
    rules: {
        "react-native/no-unused-styles": 2
    }
};
```

You should extend from a relevant ruleset for your project, and include the name of any needed plugins without the ```eslint-plugin``` prefix, e.g. ```eslint-plugin-react-native``` is written as ```plugins: ["react-native"]```. In this example we create a React Native file within which we want all of our React linting rules, so we extend ```./react.js``` as opposed to ```./eslint.js```.

### (3) Add any new plugin as a peer dependency in the package.json.
New plugins must be specified as a peer dependency, e.g.:
```javascript
// Before
"peerDependencies": {
    "eslint": "1.10.3",
    "eslint-plugin-flow-vars": "0.1.3",
    "eslint-plugin-react": "3.16.1"
}

// After
"peerDependencies": {
    "eslint": "1.10.3",
    "eslint-plugin-flow-vars": "0.1.3",
    "eslint-plugin-react": "3.16.1",
    "eslint-plugin-react-native": "1.0.0"
}
```

### (4) Test your new rule on a project.
Commit your new rule to a __new branch (not master)__, open a different project and import your commit to ```eslint-config-rnf``` in the project's package.json.

```javascript
// Example
"devDependencies": {
    "eslint-config-rnf": "git+ssh://git@stash.rnfdigital.com:7999/int/eslint-config-rnf.git#f655497c859b1608dc9de3d85943bd8ca480e10f"
}
```

Use ```npm install``` to get the new config.

If a new peer dependency has been added this will also have to be installed, e.g. ```npm install eslint-plugin-react-native```.

Additionally, if a new file has been added, the .eslintrc of that project will have to be modified to use the new file as its ruleset.
```javascript
// Before, using react.js rules
{
    "extends": "rnf/react",
}

// After, using react-native.js rules
{
    "extends": "rano/react-native"
}
```

Write some code that violates the rule and use ```npm run lint```. If you see the error violating code in the output, then your new rule works.

### (5) Update the version number of eslint-config-rnf.
Within the package.json, change the version number to be higher than before. A new rule is generally appropriate to increase the minor version number.
```javascript
// Before version update
 "version": "5.0.1",

// After version update
"version": "5.1.1",
```

### (6) Create a pull request for your rule.
Make a pull request for your new branch with intent to merge into master. Once approval is given, merge into master.

### (7) Push a tag for the updated version.
Tag the merge commit with the new version number e.g. ```v5.1.1``` and push the tag to master.

### (8) Update projects where the new rule is to be used.
Follow the same steps for updating your project as in (4), except now importing eslint-config-rnf with the new version:

```javascript
// Example
"devDependencies": {
    "eslint-config-rano": "git+ssh://git@stash.rnfdigital.com:7999/int/eslint-config-rano.git#v5.1.1"
}
```

* Import new eslint-config-rano package.
* Update .eslintrc with new file name if needed.
* npm install the new eslint-config-rnf.
* npm install any new peer dependencies that were added.

Use ```npm run lint``` to check your code, and if there are any errors, fix them! Builds will no longer pass for that project until the new lint errors have been resolved.
