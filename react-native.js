module.exports = {
    extends: ["./react.js"],
    plugins: ["react-native"],
    rules: {
        "react-native/no-unused-styles": 2
    }
};
